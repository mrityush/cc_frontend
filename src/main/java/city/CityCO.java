package city;

/**
 * Created by mj on 25/5/16.
 */
public class CityCO {
    public String name;
    public long stateId;

    public CityCO() {
    }

    public CityCO(String name, long stateId) {
        this.name = name;
        this.stateId = stateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStateId() {
        return stateId;
    }

    public void setStateId(long stateId) {
        this.stateId = stateId;
    }
}
