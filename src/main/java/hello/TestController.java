package hello;

import city.CityCO;
import dto.RequestResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import user.UserLoginCredentials;
import utils.HttpUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class TestController {

    Logger logger = LoggerFactory.getLogger(TestController.class);

    //    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping("/greeting")
    public String greeting(@RequestParam(value = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @RequestMapping("/test/2")
    public String testMestod(HttpServletRequest request, Model model) {
        String reqHeader = request.getHeader("auth");
        String token = (String) request.getSession().getAttribute("token");
        if (token != null && !token.isEmpty()) {
            model.addAttribute("name", request.getSession().getAttribute("token"));
            return "home";
        } else {
            return "login";
        }
    }

    @RequestMapping("/test/login")
    public String testLoginMestod(HttpServletRequest request, Model model) throws Exception {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        RequestResponseDTO response = HttpUtils.postData("http://localhost:8084/user/login", "",
                RequestMethod.POST.toString(), new UserLoginCredentials(username, password));
        String token = ((Map<String, String>) response.getData()).get("token");
        request.getSession().setAttribute("token", token);
        logger.info("generated token = ", token);
//        String reqHeader = request.getHeader("auth");
        model.addAttribute("name", token);
        return "greeting";
    }

    @RequestMapping("/test/api")
    public String testApi(HttpServletRequest request, Model model) throws Exception {
        String token = (String) request.getSession().getAttribute("token");
        RequestResponseDTO response = HttpUtils.postData("http://localhost:8084/city/add", token,
                RequestMethod.POST.toString(), new CityCO("Noida", 27));
        request.getSession().setAttribute("token", token);
        logger.info("generated token = ", token);
        model.addAttribute("name", response);
        return "greeting";
    }

    @RequestMapping("/test/logout")
    public String testLogoutMethod(HttpServletRequest request, Model model) throws Exception {
//        String token = (String) request.getSession().getAttribute("token");
//        String password = request.getParameter("password");
//        RequestResponseDTO response = HttpUtils.postData("http://localhost:8084/user/login", "",
//                RequestMethod.POST.toString(), new UserLoginCredentials(username, password));
        request.getSession().invalidate();
//        logger.info("generated token = ", token);
//        String reqHeader = request.getHeader("auth");
//        model.addAttribute("name", token);
        return "login";
    }

}
