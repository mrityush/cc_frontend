package user;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by niranjan on 11/9/16.
 */
public enum UserRole {
    ADMIN(0, "ADMIN"),

    VENDOR(1, "VENDOR"),

    USER(2, "USER");

    Integer value;
    String description;

    public Integer getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    UserRole(int value, String description) {
        this.value = value;
        this.description = description;
    }

    static Map<Integer, UserRole> authoritiesMap = new HashMap<>();

    static {
        for (UserRole generalStatus : UserRole.values()) {
            authoritiesMap.put(generalStatus.getValue(), generalStatus);
        }
    }

    public static UserRole valueOf(Integer enumValue) {
        return authoritiesMap.get(enumValue);
    }
}
